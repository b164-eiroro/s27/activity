const http = require('http');
const port = 6000

let users = [{
                "firstName": "Mary Jane",
                "lastName": "Dela Cruz",
                "mobileNo": "09123456789",
                "email": "mjdelacruz@mail.com",
                "password": 123
        },
        {
                "firstName": "John",
                "lastName": "Doe",
                "mobileNo": "09123456789",
                "email": "jdoe@mail.com",
                "password": 123
        }
]
const server = http.createServer((request, response) => {
	if(request.url == '/profile' && request.method == "POST"){
		let requestBody= ''
		request.on('data', function(data) {
			requestBody += data
		})

		request.on('end', function() {
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password

			}

			users.push(newUser)
			console.log(users)

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser))
			response.end()
		})
	}
});
server.listen(port);
console.log(`Server is running at localhost:${port}.`)